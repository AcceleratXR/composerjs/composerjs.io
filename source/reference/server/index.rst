=======================
API Reference (Service)
=======================

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Libraries:

   core/globals
   service-core/globals