[@composer-js/core](../README.md) › [Globals](../globals.md) › [JWTUtilsPayloadKeyOptions](jwtutilspayloadkeyoptions.md)

# Interface: JWTUtilsPayloadKeyOptions

Describes the configuration options to be used with the `JWTUtilsConfig.payload` property when performing
key-based encryption.

**`author`** Jean-Philippe Steinmetz <info@acceleratxr.com>

## Hierarchy

* [JWTUtilsPayloadOptions](jwtutilspayloadoptions.md)

  ↳ **JWTUtilsPayloadKeyOptions**

## Index

### Properties

* [compress](jwtutilspayloadkeyoptions.md#compress)
* [encrypt](jwtutilspayloadkeyoptions.md#encrypt)
* [private_key](jwtutilspayloadkeyoptions.md#private_key)
* [public_key](jwtutilspayloadkeyoptions.md#public_key)

## Properties

###  compress

• **compress**: *boolean*

*Inherited from [JWTUtilsPayloadOptions](jwtutilspayloadoptions.md).[compress](jwtutilspayloadoptions.md#compress)*

Defined in JWTUtils.ts:64

Set to `true` to enable payload compression, otherwise set to `false`.

___

###  encrypt

• **encrypt**: *boolean*

*Inherited from [JWTUtilsPayloadOptions](jwtutilspayloadoptions.md).[encrypt](jwtutilspayloadoptions.md#encrypt)*

Defined in JWTUtils.ts:69

Set to `true` to indicate that the JWT token payload is encrypted, otherwise set to `false`.

___

###  private_key

• **private_key**: *string*

Defined in JWTUtils.ts:105

The private key used to encrypt JWT token payloads.

___

###  public_key

• **public_key**: *string*

Defined in JWTUtils.ts:110

The public key used to decrypt JWT token payloads.
