[@composer-js/core](../README.md) › [Globals](../globals.md) › [JWTUser](jwtuser.md)

# Interface: JWTUser

Describes the `User` object that is encoded in the payload of a JWT token.

**`author`** Jean-Philippe Steinmetz <info@acceleratxr.com>

## Hierarchy

* **JWTUser**

## Index

### Properties

* [dateCreated](jwtuser.md#optional-datecreated)
* [dateModified](jwtuser.md#optional-datemodified)
* [email](jwtuser.md#optional-email)
* [externalIds](jwtuser.md#optional-externalids)
* [name](jwtuser.md#optional-name)
* [roles](jwtuser.md#optional-roles)
* [uid](jwtuser.md#uid)
* [verified](jwtuser.md#optional-verified)

## Properties

### `Optional` dateCreated

• **dateCreated**? : *Date*

Defined in JWTUtils.ts:22

The date and time that the user was created.

___

### `Optional` dateModified

• **dateModified**? : *Date*

Defined in JWTUtils.ts:27

The date and time that the user was last modified.

___

### `Optional` email

• **email**? : *undefined | string*

Defined in JWTUtils.ts:37

The unique e-mail address of the user.

___

### `Optional` externalIds

• **externalIds**? : *string[]*

Defined in JWTUtils.ts:47

The list of unique identifiers for each third-party platform the user is linked to.

___

### `Optional` name

• **name**? : *undefined | string*

Defined in JWTUtils.ts:32

The unique name of the user.

___

### `Optional` roles

• **roles**? : *string[]*

Defined in JWTUtils.ts:42

The list of roles (by name) that the user is apart of and will assume privileges for.

___

###  uid

• **uid**: *string*

Defined in JWTUtils.ts:17

The universally unique identifier of the user.

___

### `Optional` verified

• **verified**? : *undefined | false | true*

Defined in JWTUtils.ts:52

Indicates if the user's e-mail address has been verified.
