[@composer-js/core](../README.md) › [Globals](../globals.md) › [JWTUtilsPayloadOptions](jwtutilspayloadoptions.md)

# Interface: JWTUtilsPayloadOptions

Describes the configuration options to be used with the `JWTUtilsConfig.payload` property.

**`author`** Jean-Philippe Steinmetz <info@acceleratxr.com>

## Hierarchy

* **JWTUtilsPayloadOptions**

  ↳ [JWTUtilsPayloadPasswordOptions](jwtutilspayloadpasswordoptions.md)

  ↳ [JWTUtilsPayloadKeyOptions](jwtutilspayloadkeyoptions.md)

## Index

### Properties

* [compress](jwtutilspayloadoptions.md#compress)
* [encrypt](jwtutilspayloadoptions.md#encrypt)

## Properties

###  compress

• **compress**: *boolean*

Defined in JWTUtils.ts:64

Set to `true` to enable payload compression, otherwise set to `false`.

___

###  encrypt

• **encrypt**: *boolean*

Defined in JWTUtils.ts:69

Set to `true` to indicate that the JWT token payload is encrypted, otherwise set to `false`.
