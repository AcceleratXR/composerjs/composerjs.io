[@composer-js/core](../README.md) › [Globals](../globals.md) › [UserUtils](userutils.md)

# Class: UserUtils

Utilities for working with authenticated user objects. An user object is expected to have the following
properties.

* `uid` - Universally unique identifier for the user
* `email` - Unique e-mail address for the user
* `roles` - A list of unique names indicating the permissions of the user.
* `verified` - Indicates if the user's e-mail address has been verified.

**`author`** Jean-Philippe Steinmetz <info@acceleratxr.com>

## Hierarchy

* **UserUtils**

## Index

### Methods

* [hasRole](userutils.md#static-hasrole)
* [hasRoles](userutils.md#static-hasroles)

## Methods

### `Static` hasRole

▸ **hasRole**(`user`: any, `role`: string, `orgUid?`: undefined | string): *boolean*

Defined in UserUtils.ts:24

Returns `true` if the given user object has a role with the specified name, otherwise returns `false`.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`user` | any | The user object to inspect. |
`role` | string | The unique name of the role to search for. |
`orgUid?` | undefined &#124; string | The unique identifier of an organization whose role will be verified.  |

**Returns:** *boolean*

___

### `Static` hasRoles

▸ **hasRoles**(`user`: any, `roles`: string[], `orgUid?`: undefined | string): *boolean*

Defined in UserUtils.ts:49

Returns `true` if the given user object has at least one role from the specified list of names, otherwise returns `false`.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`user` | any | The user object to inspect. |
`roles` | string[] | A list of unique names of the roles to search for. |
`orgUid?` | undefined &#124; string | The unique identifier of an organization whose role will be verified.  |

**Returns:** *boolean*
