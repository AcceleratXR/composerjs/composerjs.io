[@composer-js/core](../README.md) › [Globals](../globals.md) › [StringUtils](stringutils.md)

# Class: StringUtils

Utility functions for working with strings.

**`author`** Jean-Philippe Steinmetz

## Hierarchy

* **StringUtils**

## Index

### Methods

* [findAndReplace](stringutils.md#static-findandreplace)
* [getParameters](stringutils.md#static-getparameters)
* [replaceAll](stringutils.md#static-replaceall)
* [toCamelCase](stringutils.md#static-tocamelcase)
* [toPascalCase](stringutils.md#static-topascalcase)

## Methods

### `Static` findAndReplace

▸ **findAndReplace**(`contents`: string, `variables`: any): *string*

Defined in StringUtils.ts:46

Performs a search and replace on the provided contents with the map of variable replacements. The contents
must use Mustache formatted tokens such as `{{toreplace}}`.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`contents` | string | The stringt to perform the find and replace on. |
`variables` | any | A map of key=>value pairs to search for and replace.  |

**Returns:** *string*

___

### `Static` getParameters

▸ **getParameters**(`str`: string): *Array‹string›*

Defined in StringUtils.ts:18

Returns a list of all parameters contained within the string. A parameter is a bracket delimited substring
(e.g. /my/{key}/with/{id}).

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`str` | string | The string to search for parameters. |

**Returns:** *Array‹string›*

A list of parameters contained in the provided string.

___

### `Static` replaceAll

▸ **replaceAll**(`str`: string, `match`: string | RegExp, `prefix`: string): *string*

Defined in StringUtils.ts:81

Replaces all instances of the match regex pattern with the contents of the inner regular expression pattern for
the given string.

e.g.
  var result = replaceAll('/my/path/{id}', new RegExp('\\{([^\\}]+)\\}'), ':');
  console.log(result); // -> /my/path/:id

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`str` | string | The string to perform replacement on. |
`match` | string &#124; RegExp | The regular expression pattern to match containing an outer and inner pattern. |
`prefix` | string | The prefix to prepend the replacement text with. |

**Returns:** *string*

The fully replaced contents of the string.

___

### `Static` toCamelCase

▸ **toCamelCase**(`str`: string): *string*

Defined in StringUtils.ts:99

Converts the first character in the given string to be lowercase (e.g. myVariable).

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`str` | string | The string to convert to camelCase. |

**Returns:** *string*

The string converted to camelCase.

___

### `Static` toPascalCase

▸ **toPascalCase**(`str`: string): *string*

Defined in StringUtils.ts:109

Converts the first character in the given string to be uppercase (e.g. MyVariable).

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`str` | string | The string to convert to PascalCase. |

**Returns:** *string*

The string converted to PascalCase.
