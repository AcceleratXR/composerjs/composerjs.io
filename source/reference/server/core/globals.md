[@composer-js/core](README.md) › [Globals](globals.md)

# @composer-js/core

## Index

### Classes

* [ClassLoader](classes/classloader.md)
* [FileUtils](classes/fileutils.md)
* [JWTUtils](classes/jwtutils.md)
* [OASUtils](classes/oasutils.md)
* [StringUtils](classes/stringutils.md)
* [ThreadPool](classes/threadpool.md)
* [UserUtils](classes/userutils.md)

### Interfaces

* [JWTUser](interfaces/jwtuser.md)
* [JWTUtilsConfig](interfaces/jwtutilsconfig.md)
* [JWTUtilsPayloadKeyOptions](interfaces/jwtutilspayloadkeyoptions.md)
* [JWTUtilsPayloadOptions](interfaces/jwtutilspayloadoptions.md)
* [JWTUtilsPayloadPasswordOptions](interfaces/jwtutilspayloadpasswordoptions.md)

### Variables

* [YAML](globals.md#const-yaml)
* [combine](globals.md#combine)
* [format](globals.md#format)
* [logFormat](globals.md#const-logformat)
* [logger](globals.md#const-logger)
* [mkdirp](globals.md#const-mkdirp)
* [os](globals.md#const-os)
* [printf](globals.md#printf)
* [readline](globals.md#const-readline)
* [timestamp](globals.md#timestamp)
* [transports](globals.md#transports)
* [winston](globals.md#const-winston)

### Functions

* [Logger](globals.md#const-logger)

## Variables

### `Const` YAML

• **YAML**: *any* =  require("js-yaml")

Defined in OASUtils.ts:10

___

###  combine

• **combine**: *any*

Defined in Logger.ts:6

___

###  format

• **format**: *any*

Defined in Logger.ts:5

___

### `Const` logFormat

• **logFormat**: *any* =  printf((info: any) => {
    return `${info.timestamp} ${info.level}: ${info.message}`;
})

Defined in Logger.ts:7

___

### `Const` logger

• **logger**: *any* =  Logger()

Defined in FileUtils.ts:11

Defined in OASUtils.ts:9

___

### `Const` mkdirp

• **mkdirp**: *Function* =  util.promisify(require("mkdirp"))

Defined in FileUtils.ts:12

___

### `Const` os

• **os**: *any* =  require("os")

Defined in threads/ThreadPool.ts:5

___

###  printf

• **printf**: *any*

Defined in Logger.ts:6

___

### `Const` readline

• **readline**: *any* =  require("readline")

Defined in FileUtils.ts:13

___

###  timestamp

• **timestamp**: *any*

Defined in Logger.ts:6

___

###  transports

• **transports**: *any*

Defined in Logger.ts:5

___

### `Const` winston

• **winston**: *any* =  require("winston")

Defined in Logger.ts:4

## Functions

### `Const` Logger

▸ **Logger**(`level`: string, `file`: string | undefined): *any*

Defined in Logger.ts:17

Creates a new logger with the specified level and file name to output logs to.

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`level` | string | "debug" | The logging level to create the logger with. |
`file` | string &#124; undefined |  undefined | The name (without an extension) of the file to output logs to.  |

**Returns:** *any*
