**[@composer-js/service-core](../README.md)**

> [Globals](../globals.md) / ACLAction

# Enumeration: ACLAction

Describes the various permission actions that can be performed against an entity.

**`author`** Jean-Philippe Steinmetz <info@acceleratxr.com>

## Index

### Enumeration members

* [CREATE](aclaction.md#create)
* [DELETE](aclaction.md#delete)
* [FULL](aclaction.md#full)
* [READ](aclaction.md#read)
* [SPECIAL](aclaction.md#special)
* [UPDATE](aclaction.md#update)

## Enumeration members

### CREATE

•  **CREATE**:  = "CREATE"

*Defined in src/security/AccessControlList.ts:11*

___

### DELETE

•  **DELETE**:  = "DELETE"

*Defined in src/security/AccessControlList.ts:12*

___

### FULL

•  **FULL**:  = "FULL"

*Defined in src/security/AccessControlList.ts:13*

___

### READ

•  **READ**:  = "READ"

*Defined in src/security/AccessControlList.ts:14*

___

### SPECIAL

•  **SPECIAL**:  = "SPECIAL"

*Defined in src/security/AccessControlList.ts:15*

___

### UPDATE

•  **UPDATE**:  = "UPDATE"

*Defined in src/security/AccessControlList.ts:16*
