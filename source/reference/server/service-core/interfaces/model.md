[@composer-js/service-core](../README.md) › [Globals](../globals.md) › [Model](model.md)

# Interface: Model

## Hierarchy

* **Model**

## Index

### Properties

* [modelClass](model.md#optional-modelclass)

## Properties

### `Optional` modelClass

• **modelClass**? : *any*

Defined in src/Server.ts:36
