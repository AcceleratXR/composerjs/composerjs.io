**[@composer-js/service-core](../README.md)**

> [Globals](../globals.md) / Entity

# Interface: Entity

## Hierarchy

* **Entity**

## Index

### Properties

* [datastore](entity.md#datastore)

## Properties

### datastore

• `Optional` **datastore**: any

*Defined in src/ObjectFactory.ts:12*
