**[@composer-js/service-core](../README.md)**

> [Globals](../globals.md) / IndexRoute

# Class: IndexRoute

The `IndexRoute` provides a default `/` endpoint the returns metadata information about the service such as
name, version.

**`author`** Jean-Philippe Steinmetz

## Hierarchy

* **IndexRoute**

## Index

### Constructors

* [constructor](indexroute.md#constructor)

### Properties

* [config](indexroute.md#config)

### Methods

* [get](indexroute.md#get)

## Constructors

### constructor

\+ **new IndexRoute**(`config`: any): [IndexRoute](indexroute.md)

*Defined in src/routes/IndexRoute.ts:15*

#### Parameters:

Name | Type |
------ | ------ |
`config` | any |

**Returns:** [IndexRoute](indexroute.md)

## Properties

### config

• `Private` **config**: any

*Defined in src/routes/IndexRoute.ts:15*

## Methods

### get

▸ `Private`**get**(): any

*Defined in src/routes/IndexRoute.ts:22*

**Returns:** any
