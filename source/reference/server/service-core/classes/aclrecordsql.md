**[@composer-js/service-core](../README.md)**

> [Globals](../globals.md) / ACLRecordSQL

# Class: ACLRecordSQL

Implementation of the `ACLRecord` interface for use with SQL databases.

## Hierarchy

* **ACLRecordSQL**

## Implements

* [ACLRecord](../interfaces/aclrecord.md)

## Index

### Constructors

* [constructor](aclrecordsql.md#constructor)

### Properties

* [create](aclrecordsql.md#create)
* [delete](aclrecordsql.md#delete)
* [full](aclrecordsql.md#full)
* [read](aclrecordsql.md#read)
* [special](aclrecordsql.md#special)
* [update](aclrecordsql.md#update)
* [userOrRoleId](aclrecordsql.md#userorroleid)

## Constructors

### constructor

\+ **new ACLRecordSQL**(`other?`: any): [ACLRecordSQL](aclrecordsql.md)

*Defined in src/security/AccessControlListSQL.ts:34*

#### Parameters:

Name | Type |
------ | ------ |
`other?` | any |

**Returns:** [ACLRecordSQL](aclrecordsql.md)

## Properties

### create

•  **create**: boolean \| null

*Implementation of [ACLRecord](../interfaces/aclrecord.md).[create](../interfaces/aclrecord.md#create)*

*Defined in src/security/AccessControlListSQL.ts:19*

___

### delete

•  **delete**: boolean \| null

*Implementation of [ACLRecord](../interfaces/aclrecord.md).[delete](../interfaces/aclrecord.md#delete)*

*Defined in src/security/AccessControlListSQL.ts:28*

___

### full

•  **full**: boolean \| null

*Implementation of [ACLRecord](../interfaces/aclrecord.md).[full](../interfaces/aclrecord.md#full)*

*Defined in src/security/AccessControlListSQL.ts:34*

___

### read

•  **read**: boolean \| null

*Implementation of [ACLRecord](../interfaces/aclrecord.md).[read](../interfaces/aclrecord.md#read)*

*Defined in src/security/AccessControlListSQL.ts:22*

___

### special

•  **special**: boolean \| null

*Implementation of [ACLRecord](../interfaces/aclrecord.md).[special](../interfaces/aclrecord.md#special)*

*Defined in src/security/AccessControlListSQL.ts:31*

___

### update

•  **update**: boolean \| null

*Implementation of [ACLRecord](../interfaces/aclrecord.md).[update](../interfaces/aclrecord.md#update)*

*Defined in src/security/AccessControlListSQL.ts:25*

___

### userOrRoleId

•  **userOrRoleId**: string

*Implementation of [ACLRecord](../interfaces/aclrecord.md).[userOrRoleId](../interfaces/aclrecord.md#userorroleid)*

*Defined in src/security/AccessControlListSQL.ts:16*
