==============
Authentication
==============

All Composer projects leverage JSON Web Tokens (JWT) as the primary mechanism for user authentication due to its speed,
flexibility and security robustness. As Composer is first and foremost a framework that was designed for building
highly scalable, distributed, micro-services and platforms the distributed nature of JWT makes it an easy choice as
it eliminates any cross-talk to a central authentication server to validate incoming requests from a user.

JWT Payload
===========

In order for this to work correctly, all JWT tokens must be formatted in a particular way in order for Composer
services to be able to decode them properly. As such the token's payload must contain a ``profile`` property
which contains at least a ``uid`` to uniquely identify the user as well as an optional list of ``roles`` which
will determine the available permissions the user has to perform actions within the service.

.. code-block:: json

    {
        "profile": {
            "uid": "3b61f36b-8254-4a7a-ae36-33459692127d",
            "roles": []
        }
    }

Different services may choose to require additional properties within the ``profile`` but these are the ones
Composer uses internally to perform all of its work.

Optionally, a token's ``profile`` payload may be encrypted to provide an additional level of security.

Server Authentication
=====================

By default, all Composer services come with a built-in authentication handler for JWT authentication tokens.
Tokens may be passed to the service by one of three methods; the Authorization header, the cookie header,
or the ``jwt_token`` query parameter.

Authorization Header
~~~~~~~~~~~~~~~~~~~~

A Composer service will automatically decode a JWT token and attempt to validate it when an ``Authorization`` header
is included with a request containing either the ``Bearer`` or ``jwt`` type indicator.

**Example**

.. code-block:: http
    :emphasize-lines: 3

    GET /path HTTP/1.1
    Host: composerjs.io
    Authorization: Bearer 0938r2098n209rq38v90bb87209830928r0q3809r8302vqr803q28r0982q0

.. code-block:: http
    :emphasize-lines: 3

    GET /path HTTP/1.1
    Host: composerjs.io
    Authorization: jwt 0938r2098n209rq38v90bb87209830928r0q3809r8302vqr803q28r0982q0

Cookie Header
~~~~~~~~~~~~~

Composer also recognizes cookies with the name ``jwt`` that are passed in to the HTTP request.

**Example**

.. code-block:: http
    :emphasize-lines: 3

    GET /path HTTP/1.1
    Host: composerjs.io
    Cookie: jwt=0938r2098n209rq38v90bb87209830928r0q3809r8302vqr803q28r0982q0

Query Parameter
~~~~~~~~~~~~~~~

Sometimes it is not possible to pass the authentication token via a header. This is common in debugging scenarios or on
platforms that don't allow custom headers for HTTP clients. In such a case you can also pass in the token using the
special ``jwt_token`` query parameter.

**Example**

.. code-block:: http
    :emphasize-lines: 1

    GET /path?jwt_token=0938r2098n209rq38v90bb87209830928r0q3809r8302vqr803q28r0982q0 HTTP/1.1
    Host: composerjs.io