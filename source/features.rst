========
Features
========

Composer comes with a powerful feature set out of the box and is easily extended to provide additional capability.

* Built on OpenAPI
* HTTP 1.x/2.x Web Server
* WebSocket Support
* Global Configuration
* Dependency Injection
* Built-in behaviors for common REST API actions
* Built-in ORM layer
* Document Version Tracking
* MongoDB support
* Redis support
* 2nd Level Caching
* Authentication
* Role Based Access Control
* Prometheseus metrics