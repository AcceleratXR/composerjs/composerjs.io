============
Installation
============

Installing Composer is very easy. From any terminal or command line simply run the following command.

.. code-block:: bash
   :linenos:

   yarn global add @composer-js/cli

Once complete can now run ``composer``.

.. code-block:: bash
   :linenos:

   composer --help

