=======
Caching
=======

Composer offers built-in 2nd level caching for all database operations using **redis**. By default the feature is
disabled and must be enabled in the configuration (see ``datastores.cache`` in the `Configuration <configuration>`_ page).

In order for a data model to be cached it must use the ``@Cache`` decorator in the class definition.

.. code-block:: typescript
   :linenos:
   :emphasize-lines: 2

   @Entity()
   @Cache()
   @Unique(["uid", "id"])
   export default class Order extends BaseMongoEntity {
   }

The ``@Cache`` decorator takes an optional ``ttl`` argument that indicates the time, in seconds, that an object will
remain in the cache. The default value is ``30`` seconds.

Query Caching
=============

Route handlers that extend from ``ModelRoute`` and use the ``doCount`` and ``doFindAll`` functions leverage query
caching. These functions will cache the results of any query that has been requested by the service for a configured
duration of time and then rebuild the cached results once the time has expired.

Object Caching
==============

For route handlers that extend ``ModelRoute`` and use the ``doUpdate`` and ``doFindById`` functions the object cache
is used. Any time an object is initially access (via create, find or other) it will be placed in the cache for faster
subsequent retrieval.
