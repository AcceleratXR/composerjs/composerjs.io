=================
Project Structure
=================

Once you've generated your NodeJS server project with Composer you'll want to familiarize yourself with its structure.

.. image:: /images/server_project_structure.PNG

The majority of the important bits are in the ``src`` folder. So let's focus on that first.

``src/jobs``
============

The ``jobs`` folder contains TypeScript classes that provide background services that run at a scheduled interval. Each
Composer project comes with one default background job called the ``MetricsCollector``. The server will automatically
pick up any class within this folder and load it as a background job on startup.

``src/models``
==============

At startup the server will automatically scan and load all files defined in this folder as data model definitions.
Each data model class must be decorated with Composer and TypeORM decorators as Composer uses the TypeORM system
to manage storage of all data model objects. Composer will have automatically generated all data models defined
in the OpenAPI specification as schemas, adding the desired decorators that were specified in the OpenAPI file.

Each generated model class also features a constructor that takes a single object as its sole argument. This serves
as a simple copy constructor which enforces the desired structure of the data model, throwing away any additional
undesired data that is passed to it.

``src/routes``
==============

The ``routes`` folder is where most of the magic happens. Each file in this folder is automatically loaded by the server
at startup as well. Each file corresponds to one or more Path Item objects in the OpenAPI specification file. When the
``x-schema`` field is used for a Path Item, then all corresponding path items associated with a given data model are
grouped into one class named ``<Schema>Route.ts``. If the ``x-name`` was used then all corresponding path items that
specified the same name are grouped into a single class named ``<Name>Route.ts``.

``src/config.ts``
=================

The ``config.ts`` file is where all the global application configuration is stored. This contains everything from the
background jobs to the datastores and JWT authentication information. The server leverages the
`nconf <https://www.npmjs.com/package/nconf>`_ configuration system to make defining and accessing configuration
variables simple. The resulting ``config`` object is automatically injected into each **Route** class via the ``@Config``
decorator.

``src/server.ts``
=================

The ``server.ts`` file is the entry point of the server and is the first script run. It contains simple and common
functionality needed to initialize the global configuration, system logger, background service manager and the
Composer Server itself.

``test``
========

The ``test`` folder contains all unit tests for the project. Composer is pre-configured to use the
`jest <https://jestjs.io/>`_ unit test framework. Composer automatically generates unit test files for each file in the
``src/routes`` folder. However, these tests are not complete and must be modified.