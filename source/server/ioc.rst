==========================
Dependency Injection (IoC)
==========================

Composer includes a dependency injection system for implementing Inversion of Control (IOC).
This is accomplished through the use of the `ObjectFactory` which is constructed at application
startup.

The `ObjectFactory` class is used interally by the Server to automatically construct and initialize
all route handlers and background jobs, including their dependencies. It is also capable of managing
the object lifecycle including initialization and destruction.

This system uses decorators. Decorators are used to identify individual class members that should be injected
including what type of object they are as well as functions that contribute to the management of the object's
lifecycle. There are also a few special decorators used to inject common variable types such as application
configuration variables or the application logging utility.

Object Lifecycle
================

All objects constructed by the `ObjectFactory` are given a unique identifying instance name. This can be
specified at object construction using the `newInstance` function or via the `@Inject` decorator. By default,
if any object is constructed without a name specified, the system will name the object `default`.

To construct a new object using the `ObjectFactory` simply use the `newInstance` function as in the following example.

.. code-block:: TypeScript
   :linenos:
   :emphasize-lines: 23

   class MyClass {
       private var1: any;
       private var2: any;
       privaet var3: any;

       constructor(arg1: any, arg2: any, arg3: any) {
           this.var1 = arg1;
           this.var2 = arg2;
           this.var3 = arg3;
       }

       @Init
       private init(): void {
           // This function is called after instantiation and once all variables have been injected
       }

       @Destroy
       private destroy(): void {
           // This function is called when the object is about to be destroyed.
       }
   }

   const myObject: MyClass = objectFactory.newInstance<MyClass>(MyClass, "default", arg1, arg2, arg3);

In the above example we instantiate a new instance of class `MyClass` and provide the constructor the arguments `arg1`, `arg2`, and `arg3`.
Two functions are also defined; the `init` and `destroy` functions. These functions are used to notify the object that construction is
complete and that all dependencies have been injected. The `destroy` function is called when the system is about to destroy the object
instance and should perform any last minute cleanup. If either function returns a `Promise` the system will wait until the completion
of the async operation before continuing.

Dependency Injection
====================

Dependency injection is performed by decorating member variables of a class with the `@Inject` decorator or one of the other
specialized decorators as shown in the example below.

.. code-block:: TypeScript
   :linenos:
   :emphasize-lines: 23

   class MyClass {
       @Config()
       private config?: any;

       @Config("path:to:flag")
       private myFlag: boolean = false;

       @Logger
       private logger?: any;

       @Inject(MySecondClass)
       private mySecond?: MySecondClass;

       @Inject(MySecondClass, "custom", 1, 2, 3)
       private mySecond?: MySecondClass;

       constructor(arg1: any, arg2: any, arg3: any) {
           this.var1 = arg1;
           this.var2 = arg2;
           this.var3 = arg3;
       }
   }

In the above example several concepts are demonstrated. The first is the use of the `@Config` decorator. This decorator
will inject the application's global configuration object (line 2) or a specific variable of the global configuration
object when a path is given (line 5).

Next the `@Logger` decorator is used on line 8. This tells the system to inject the application's logging utility.

The last two varibles use the standard `@Inject` decorator to inject a variable by class type and name. In line 11, only
the class type is specified which results in the system looking up the object with the name `default` for that class type.
When no existing instance for the name is found it is automatically constructed and injected. Since no arguments are
specified it is expected that the `MySecondClass` does not require any arguments in it's constructor. The injection of
line 13 on the other hand specifies both a name, `custom`, and a set of constructor arguments. The system will first
look up and existing instance for the given name and if not found instantiate the object with the given constructor
arguments (e.g. `1, 2, 3`).

General Decorators
==================

Below is the list of decorators available to the use for general purpose dependencies. They are exposed via the
`ObjectDecorators` interface import.

.. code-block:: TypeScript
   :linenos:

   import { ObjectDecorators } from "@composer-js/service-core";
   const { Config, Destroy, Init, Inject, Logger } = ObjectDecorators;

``@Init``
~~~~~~~~~

The ``@Init`` decorator is used to indicate the function that will be called immediately after an object has been
instantiated and all dependencies injected.

.. code-block:: TypeScript
   :linenos:
   :emphasize-lines: 2

    class MyClass {
       @Init
       private init(): void {
           // This function is called after instantiation and once all variables have been injected
       }

       @Destroy
       private destroy(): void {
           // This function is called when the object is about to be destroyed.
       }
   }

``@Destroy``
~~~~~~~~~~~~

The ``@Destroy`` decorator is used to indicate the function that will perform any cleanup before the object is destroyed.

.. code-block:: TypeScript
   :linenos:
   :emphasize-lines: 17

    class MyClass {
       private var1: any;
       private var2: any;
       privaet var3: any;

       constructor(arg1: any, arg2: any, arg3: any) {
           this.var1 = arg1;
           this.var2 = arg2;
           this.var3 = arg3;
       }

       @Init
       private init(): void {
           // This function is called after instantiation and once all variables have been injected
       }

       @Destroy
       private destroy(): void {
           // This function is called when the object is about to be destroyed.
       }
   }

``@Inject``
~~~~~~~~~~~

The ``@Inject`` decorator tells the system to automatically inject the object instance of the given type and name. If
no existing object with that name and type is found it is automatically constructed using the provided constructor
arguments.

.. code-block:: TypeScript
   :linenos:
   :emphasize-lines: 2-6

    class MyClass {
       @Inject(MySecondClass)
       private mySecond?: MySecondClass;

       @Inject(MySecondClass, "custom", 1, 2, 3)
       private mySecond?: MySecondClass;

       constructor(arg1: any, arg2: any, arg3: any) {
           this.var1 = arg1;
           this.var2 = arg2;
           this.var3 = arg3;
       }
   }

``@Config``
~~~~~~~~~~~

The ``@Config`` decorator tells the system to automatically inject the global application configuration object. The
decorator optionally takes a path to the variable desired to inject.

.. code-block:: TypeScript
   :linenos:
   :emphasize-lines: 2-6

    class MyClass {
       @Config()
       private config?: any;

       @Config("path:to:flag")
       private myFlag: boolean = false;

       constructor(arg1: any, arg2: any, arg3: any) {
           this.var1 = arg1;
           this.var2 = arg2;
           this.var3 = arg3;
       }
   }

``@Logger``
~~~~~~~~~~~

The ``@Logger`` decorator tells the system to automatically inject the application logging utility.

.. code-block:: TypeScript
   :linenos:
   :emphasize-lines: 2-3

    class MyClass {
       @Logger
       private logger?: any;

       constructor(arg1: any, arg2: any, arg3: any) {
           this.var1 = arg1;
           this.var2 = arg2;
           this.var3 = arg3;
       }
   }

Route Decorators
================

Below is the list of decorators available to the use for route handlers. They are exposed via the `RouteDecorators`
interface import.

.. code-block:: TypeScript
   :linenos:

   import { RouteDecorators } from "@composer-js/service-core";
   const { MongoRepository, Repository, RedisConnection } = RouteDecorators;

``@MongoRepository``
~~~~~~~~~~~~~~~~~~~~

The ``@MongoRepository`` decorator is inject the Mongo Repository connection for the given class type.

.. code-block:: TypeScript
   :linenos:
   :emphasize-lines: 2

    class MyClass {
       @MongoRepository(MyClass)
       private repo?: Repo<MyClass>;
   }

``@Repository``
~~~~~~~~~~~~~~~

The ``@Repository`` decorator is inject the SQL Repository connection for the given class type.

.. code-block:: TypeScript
   :linenos:
   :emphasize-lines: 2

    class MyClass {
       @Repository(MyClass)
       private repo?: Repo<MyClass>;
   }

``@RedisConnection``
~~~~~~~~~~~~~~~~~~~~

The ``@RedisConnection`` decorator is inject the redis connection for the given name.

.. code-block:: TypeScript
   :linenos:
   :emphasize-lines: 2

    class MyClass {
       @RedisConnection("default")
       private redis?: Redis;
   }