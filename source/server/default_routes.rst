==============
Default Routes
==============

Composer server projects come with a selection of default routes that are provided out-of-the-box. These include:

* Access Control Lists
* Default Index
* OpenAPI documentation (HTML / json)
* Prometheus metrics

Access Control Lists
====================

When ACLs are enabled (via ``config.ts``) the server automatically registers the ``/acls`` route handler. This route
handler responds to the standard built-in operations for the endpoints for all requests sent to the ``/acls`` path
as follows.

* ``GET /acls`` - Searches all ACLs
* ``POST /acls`` - Creates a new ACL
* ``DELETE /acls`` - Deletes all ACLs
* ``GET /acls/count`` - Searches all ACLs and return the count
* ``GET /acls/:id`` - Returns a single existing ACL
* ``PUT /acls/:id`` - Updates a single existing ACL
* ``DELETE /acls/:id`` - Deletes a single existing ACL

When the server starts up, the system automatically scans all route handler classes that inherit from ``ModelRoute``
and retrieve their ``defaultACL``. This default ACL is associated with all collection oriented requests for a given
data model (e.g. ``create``, ``findAll``, ``truncate``) where the ``uid`` of the ACL is the name of the data model
class.

Individual record ACL objects are associated with their actual objects by using the ``uid`` value for both the object
and the ACL itself. These objects will then specify the type ACL as it's parent, in order to inherit default behavior
from the class.

Default Index
=============

The default index route handler binds to the ``/`` endpoint and handles all ``GET`` requests. It is a simple handler
that returns basic information about the service such as its name, version and server time. The resulting output
looks like this.

.. code-block:: json

    {"name":"petstore","time":"2020-05-29T21:09:36.123Z","version":"1.0.0"}

OpenAPI Documentation
=====================

The OpenAPI default route provides access to the server's OpenAPI specification in both HTML and json form.

* ``GET /api-docs`` - Returns the server's OpenAPI specification in HTML format
* ``GET /openapi.json`` - Returns the server's OpenAPI specification in JSON format

Prometheus Metrics
==================

The Composer server comes with built-in support for Prometheus metrics and is exposed via the ``/metrics`` endpoint.

* ``GET /metrics`` - Returns all Prometheus metrics for the server
* ``GET /metrics/:name`` - Returns the Prometheus metric with the given name