=============
Authorization
=============

Users
=====

When a user has successfully authenticated with a Composer service, a ``user`` object, can be passed into each route
handler function using the ``@AuthUser`` decorator. The class type of the value is ``JWTUser``. This object contains
useful information about the given user such as their unique identifier (``uid``) and their associated ``roles``.

The user object may optionally contain other additional information such as the email address, name, etc. The inclusion
of this information is dependent upon the service that generated the JWT token containing the user data in the first
place. Typically, it is not necessary to have information other than the user's ``uid`` and set of ``roles``.

Roles
=====

Roles are mechanism in Composer that allows for users in to inherit permissions of a larger group. This is useful when
describing large organizations of users with a variety of different privileges and permissions.

A single default group is always assumed in every Composer project, ``admin``. The ``admin`` is considered to be
a Trusted Role.

Trusted Roles
=============

A Trusted Role in Composer is a special role, as defined in the ``config.ts`` file, which has complete and total
permission to perform any action with the service. It is a superuser and should be treated as such. Each service
can configure one or more of these trusted roles. When a trusted role is set, it supersedes any other authorization
mechanism, including Access Control Lists.

By default, all Composer projects are generated with a single trusted role named ``admin``. Therefore, any
authenticated user with ``admin`` listed in their ``roles`` list will have superuser privileges.

Access Control Lists
====================

Access Control Lists are the primary mechanism for managing user rights and access in Composer server projects. With
ACLs it is possible to provide detailed levels of control on a per URL, per data type and per object record basis.

Each ACL is associated by a ``uid``. The ``uid`` can be a real `UUID <https://en.wikipedia.org/wiki/Universally_unique_identifier>`_,
an arbitrary name (such as the class name), or a URL pattern (e.g. ``/pets``). The ACL also contains a list of
``ACLRecord`` objects. These records contain the permission information for a single individual or role for the
ACL object in question.

In Composer, ACLs are an **ALWAYS ALLOW** based system. Meaning, if no explicit ACL or record is defined for a given
user or role, it is assumed that the action is allowed to be performed. Likewise, if a ACL record exists for a given
user or role but the particular action being tested is defined with a ``null`` value, then **always allow** is assumed
and the user will be granted permission to perform the action.

There are several actions definable for a single user or role in an ACL. They are:

+-------------+---------------------------------------------------------------------------------------+
| Action      | Description                                                                           |
+=============+=======================================================================================+
| ``CREATE``  | The user or role can create a new resource.                                           |
+-------------+---------------------------------------------------------------------------------------+
| ``READ``    | The user or role can read the resource.                                               |
+-------------+---------------------------------------------------------------------------------------+
| ``UPDATE``  | The user or role can modify existing resource.                                        |
+-------------+---------------------------------------------------------------------------------------+
| ``DELETE``  | The user or role can delete existing resource.                                        |
+-------------+---------------------------------------------------------------------------------------+
| ``SPECIAL`` | The user or role has special prilieges to edit the ACL permissions.                   |
+-------------+---------------------------------------------------------------------------------------+
| ``FULL``    | The user or role has total control over the resource and supersedes any of the above. |
+-------------+---------------------------------------------------------------------------------------+

Inheritance
~~~~~~~~~~~

It is possible for an ACL to inherit the permissions from another by specifying a ``parentUid``. When a
``parentUid`` is specified, the system will first scan all ACL records for a matching entry for a given
user or role. If none can be found, it searches the parent's list of ACL records and so on. There are no
limits to the levels of parents that can be described in a given tree of ACLs. The system will stop searching
once it reaches a parent with a ``null`` value for the ``parentUid``.

Per URL
~~~~~~~

Composer supports the ability to define ACLs on a per URL pattern basis. When creating a URL based ACL the
``uid`` specified must be a valid URL regular expression pattern.

**Example**

.. code-block:: javascript

    /\/pets.*/

When a request arrives for any URL that matches a given URL it will be verified against the defined ACL.

ACL actions map directly to HTTP methods as follows:

+------------+-------------+
| HTTP Method | ACL Action |
+=============+============+
| ``POST``    | ``CREATE`` |
+------------+-------------+
| ``GET``     | ``READ``   |
+------------+-------------+
| ``PUT``     | ``UPDATE`` |
+------------+-------------+
| ``DELETE``  | ``DELETE`` |
+------------+-------------+

If there exists more than one ACL that matches to a given URL pattern, the server will select the longest
pattern to use during authorization.

**Example**

Given two ACLs for the following URL patterns:

* ``/\/pets.*/``
* ``/\/pets\/breeds.*/``

When a request arrives for the path ``/pets/breeds/collie`` the second ACL will be selected (``/\/pets\/breeds.*/``).

Should a request arrive for the path ``/pets/scotty`` then the first ACL will be selected (``/\/pets.*/``).

Per Data Type
~~~~~~~~~~~~~

Often it is desirable to control access to individual data types. Composer provides a built-in mechanism for handling
this with the ``ModelRoute`` abstract base class by defining the function ``getDefaultACL``. As a matter of
convenience, the Composer code generator will automatically create a default implementation for your data types in
which all users have read-only access to the resource. This is easily customized by adding new records and adjusting
the pre-defined ones that are created.

The built-in route handler functions provided with ``ModelRoute`` also make use of this default ACL. See the `Built-ins <builtins>`_
page for more information.

Per Object
~~~~~~~~~~

Finally, it is possible to define Access Control Lists per object record. Once again, the ``ModelRoute`` abstract base
class provides built-in behavior that automatically creates a new ACL for each object that is created with ``doCreate``
and is validated against for each built-in that operates on a single record.