=========
Debugging
=========

While it is perfectly possible to run the command ``yarn start`` to get the server going, this also requires that
you've got a database up and running locally as well. This is obviously not ideal so Composer comes pre-packaged
with configuration for Docker. With Docker you can run the service in a container, including all required databases
from the command prompt.

Docker Compose
==============

.. code-block:: bash
   :linenos:

   docker-compose build
   docker-compose run

Debugging in Visual Studio Code
===============================

Composer also is designed for work with Visual Studio Code out of the box. If you are using VS Code all you need to do
is hit ``F5`` once your docker container is up and running. It will automatically connect to your instance and allow
for debugging of the service directly.