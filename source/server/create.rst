=================
Creating a Server
=================

The first thing to do is a create the OpenAPI specification file needed to describe the Composer server.

`Click here for an example <https://gitlab.com/AcceleratXR/composerjs/cli/-/raw/master/test/petstore.yaml>`_.

Once your spec file is complete, simply run it through the Composer CLI tool, specifying ``nodejs`` as the language
and ``server`` as the type.

.. code-block:: bash
   :linenos:

   composer -i ./service.yaml -o . -t server -l nodejs

In the above example we defined our schema in the file ``service.yaml`` and told Composer to generate a new project
in the current directory.

Building the Server
===================

Before you can build the server project you will need to install the project dependencies.

.. code-block:: bash
   :linenos:

   yarn install

After that simply run ``yarn build``.

.. code-block:: bash
   :linenos:

   yarn build

Running the Server
==================

All Composer projects come pre-configured to be run with `Docker Compose <https://docs.docker.com/compose/>`_.

.. code-block:: bash
   :linenos:

   docker-compose build
   docker-compose run

Once the system is up and running you'll see log output such as the following.

.. code-block:: bash
   :linenos:

   server_1   | 2020-05-30T23:35:10.725Z info: Registered Route: GET /
   server_1   | 2020-05-30T23:35:10.726Z info: Registered Route: POST /acls
   server_1   | 2020-05-30T23:35:10.726Z info: Registered Route: DELETE /acls/:id
   server_1   | 2020-05-30T23:35:10.726Z info: Registered Route: GET /acls
   server_1   | 2020-05-30T23:35:10.726Z info: Registered Route: GET /acls/:id
   server_1   | 2020-05-30T23:35:10.726Z info: Registered Route: PUT /acls/:id
   server_1   | 2020-05-30T23:35:10.729Z info: Registered Route: GET /metrics
   server_1   | 2020-05-30T23:35:10.729Z info: Registered Route: GET /metrics/:metric
   server_1   | 2020-05-30T23:35:10.729Z info: Scanning for routes...
   server_1   | 2020-05-30T23:35:10.741Z info: Registered Route: GET /user/login
   server_1   | 2020-05-30T23:35:10.742Z info: Registered Route: GET /user/login/user/logout
   server_1   | 2020-05-30T23:35:10.742Z info: Registered Route: GET /store/order
   server_1   | 2020-05-30T23:35:10.742Z info: Registered Route: POST /store/order
   server_1   | 2020-05-30T23:35:10.743Z info: Registered Route: GET /store/order/:id
   server_1   | 2020-05-30T23:35:10.743Z info: Registered Route: PUT /store/order/:id
   server_1   | 2020-05-30T23:35:10.743Z info: Registered Route: DELETE /store/order/:id
   server_1   | 2020-05-30T23:35:10.744Z info: Registered Route: GET /pet
   server_1   | 2020-05-30T23:35:10.744Z info: Registered Route: POST /pet
   server_1   | 2020-05-30T23:35:10.744Z info: Registered Route: DELETE /pet
   server_1   | 2020-05-30T23:35:10.744Z info: Registered Route: GET /pet/:id
   server_1   | 2020-05-30T23:35:10.744Z info: Registered Route: PUT /pet/:id
   server_1   | 2020-05-30T23:35:10.744Z info: Registered Route: DELETE /pet/:id
   server_1   | 2020-05-30T23:35:10.745Z info: Registered Route: GET /user
   server_1   | 2020-05-30T23:35:10.745Z info: Registered Route: POST /user
   server_1   | 2020-05-30T23:35:10.745Z info: Registered Route: DELETE /user
   server_1   | 2020-05-30T23:35:10.746Z info: Registered Route: GET /user/:id
   server_1   | 2020-05-30T23:35:10.746Z info: Registered Route: PUT /user/:id
   server_1   | 2020-05-30T23:35:10.746Z info: Registered Route: DELETE /user/:id
   server_1   | 2020-05-30T23:35:10.746Z info: Registered Route: GET /user/count
   server_1   | 2020-05-30T23:35:10.746Z info: Initializing routes...
   server_1   | 2020-05-30T23:35:10.778Z info: Listening on port 3000...
   server_1   | 2020-05-30T23:35:10.784Z info: Starting service MetricsCollector...
