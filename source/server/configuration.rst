=============
Configuration
=============

Configuration in Composer projects utilizes the `nconf <https://www.npmjs.com/package/nconf>`_ configuration system
with all settings being stored in the ``config.ts`` file. This makes it easy to provide configuration to the server
that is easy to override both at the command line level and via environment variables.

Here is a typical configuration file for a Composer project.

.. code-block:: typescript
   :linenos:

   const packageInfo = require("../package.json");
    const conf = require("nconf")
        .argv()
        .env({
            separator: "__",
            parseValues: true,
        });

    conf.defaults({
        service_name: packageInfo.name, // The name of the service
        version: packageInfo.version, // The published version string
        cookie_secret: "COOKIE_SECRET",
        cors: {
            origin: ["http://localhost:3000"],
        },
        datastores: { // Database configuration
            acl: { // Database for Access Control Lists
                type: "mongodb",
                url: "mongodb://localhost",
                database: "acl",
                useNewUrlParser: true,
                useUnifiedTopology: true,
                synchronize: true,
            },
            // Uncomment the following to enable 2nd level caching support.
            // Only entity models with the @Cache decorator will be cached.
            //cache: {
            //    type: "redis",
            //    url: "redis://localhost:6379",
            //},
            mongodb: {
                type: "mongodb",
                url: "mongodb://localhost",
                database: "",
                useNewUrlParser: true,
                useUnifiedTopology: true,
                synchronize: true,
            },
        },
        // Specifies the role names that are considered to be trusted with administrative privileges.
        trusted_roles: ["admin"],
        // Settings pertaining to the signing and verification of authentication tokens
        auth: {
            // The default PassportJS authentication strategy to use
            strategy: "JWTStrategy",
            // The password to be used when verifying authentication tokens
            password: "MyPasswordIsSecure",
            options: {
                //"algorithm": "HS256",
                expiresIn: "7 days",
                audience: "mydomain.com",
                issuer: "api.mydomain.com",
            },
        },
        jobs: {
            defaultSchedule: "* * * * * *",
            MetricsCollector: {
                schedule: "*/5 * * * * *",
            },
        },
        session: {
            secret: "SESSION_SECRET",
        },
    });

    export default conf;

Datastores
==========

Of particular importance is the ``datastores`` property of the configuration. This takes a map of TypeORM compatible
database configurations. Additionally, a ``type`` propertly must be set for each one to indicate the type of database
to connect to. It is possible to specify as many connections as is desired.

Any database supported by TypeORM is allowed, including some of these popular ones.

+----------------+---------------------+
| Type           | Database            |
+================+=====================+
| mongodb        | MongoDB             |
+----------------+---------------------+
| postgres       | PostgreSQL          |
+----------------+---------------------+
| mysql          | MySQL               |
+----------------+---------------------+
| sqlite         | SQLite              |
+----------------+---------------------+
| redis          | REDIS               |
+----------------+---------------------+

Note the last entry in the list, ``redis``. ``redis`` is not directly supported by TypeORM but is supported by
Composer. Redis support in Composer does not offer any ORM support and instead gives direct access to a redis
client connection.

Trusted Roles
=============

The ``trusted_roles`` configuration property indicates which authenticated role names will be given superuser
privileges in the service. See `Authorization <authorization>`_ for more details.

Auth
====

The ``auth`` configuration property controls what primary authentication strategy is used for all client requests. By
default this is ``JWTStrategy`` and should not be changed. It is important to change the ``password`` and ``options``
properties before you launch your service into production.

Jobs
====

The ``jobs`` section describes the configuration for the various background services that are desired to be run with
the server. See `Background Services <jobs>`_ for more information.