=========
Built-Ins
=========

Built-ins are a way of providing default behavior for common operations in a RESTful API. In the case of Composer,
there exists one primary set of built-in functions for working with data models and is provided by the ``ModelRoute``
abstract base class.

``ModelRoute``
==============

The ``ModelRoute`` base class provides a set of route handler built-ins for common operations when designing
data oriented RESTful API services. These built-ins can be called from any route handler function to provide the
desired behavior during request processing.

These built-ins are designed around standard CRUD operations and include the following.

* ``doCount`` - Searches a collection of objects and returns the count of matches
* ``doCreate`` - Create a new object
* ``doDelete`` - Delete an existing object
* ``doFindAll`` - Search and retrieve a collection of objects
* ``doFindById`` - Retrieve an existing single object
* ``doTruncate`` - Delete all objects from a collection
* ``doUpdate`` - Update an existing object

``doCount``
~~~~~~~~~~~

.. code-block:: TypeScript

    doCount(params: any, query: any, user?: any): Promise<Count>

This built-in provides a way to perform a search on a collection of objects and return only the total count of those
matching the given criteria. It takes both the request ``params`` as provided by the ``@Param`` decorator and the
path query parameters as provided by the ``@Query`` decorator. When ACLs are enabled this built-in will also verify
that the authorized user has ``READ`` permission on the associated type ACL.

``doCreate``
~~~~~~~~~~~~

.. code-block:: TypeScript

    doCreate(obj: T, user?: any, acl?: AccessControlList): Promise<T>

This built-in is used to create a single new object and store it in the database. Once stored in the database, the
resulting object as it was stored is returned to be delivered back to the client. When ACLs are enabled this 
built-in will also verify that the authorized user has ``CREATE`` permission on the associated type ACL. The built-in
will also create an ACL for the object, inheriting the permissions of the type ACL and giving full control to the
authorized user performing the create. You may optionally provide your own ACL for the object to be created which
overrides this default behavior.

``doDelete``
~~~~~~~~~~~~

.. code-block:: TypeScript

    doDelete(id: string, user?: any): Promise<void>

The ``doDelete`` built-in is for deleting a single object in a collection. It takes a single ``id`` parameter which is
the unique identifier of the object in question. The ``id`` can be any value from an data model class that has the
``@Identifier`` decorator. This is very useful when data models have multiple possible identifiers such as a ``uid``
or a unique ``name``. When ACLs are enabled this built-in will also verify that the authorized user has ``DELETE``
permission on the associated object's ACL.

``doFindAll``
~~~~~~~~~~~~~

.. code-block:: TypeScript

    doFindAll(params: any, query: any, user?: any): Promise<T[]>

The ``doFindAll`` built-in searches collection of objects for a given set of criteria as specified by the ``params``
and ``query`` arguments. Both ``params`` and ``query`` arguments must be a map of key=value pairs where the key
corresponds to a property within the data model and the value is the desired value to find in the collection. When ACLs
are enabled this built-in will also verify that the authorized user has ``READ`` permission on the associated type ACL.

``doFindById``
~~~~~~~~~~~~~~

.. code-block:: TypeScript

    doFindById(id: string, user?: any): Promise<T | undefined>

This built-in retrieves a single existing object from a collection with a specified unique ``id``. The ``id`` can be
any value from an data model class that has the ``@Identifier`` decorator. This is very useful when data models have
multiple possible identifiers such as a ``uid`` or a unique ``name``. When ACLs are enabled this built-in will also
verify that the authorized user has ``READ`` permission on the associated object's ACL.

``doTruncate``
~~~~~~~~~~~~~~

.. code-block:: TypeScript

    doTruncate(user?: any): Promise<void>

This built-in is used to delete all objects of a single collection. It is equivalent to a DROP operation on a table.
When ACLs are enabled this built-in will also verify that the authorized user has ``DELETE`` permission on the
associated type ACL.

``doUpdate``
~~~~~~~~~~~~

.. code-block:: TypeScript

    doUpdate(id: string, obj: T, user?: any): Promise<T>

The ``doUpdate`` built-in will modify an existing record in the datastore for the object with the specified id. The
``id`` can be any value from an data model class that has the ``@Identifier`` decorator. The ``obj`` argument is the
contents of the new object to persist. It must be the **entire** object and not a partial. The system leverages an
optimistic locking mechanism to ensure that only objects with a like ``version`` can be updated. Thus, if the
``version`` of the specified object does not match the value of the existing object in the datastore, a
``409 CONFLICT`` error is thrown and returned to the client. When ACLs are enabled this built-in will also
verify that the authorized user has ``UPDATE`` permission on the associated object's ACL.