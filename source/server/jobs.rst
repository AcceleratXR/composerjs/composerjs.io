===================
Background Services
===================

Composer provides a simple mechanism for running asynchronous jobs as background services. Each background service job
is defined as a separate class in the ``jobs`` source folder. On startup, the server will scan this directory and
initialize each job that is defined in the ``config.ts`` with an accompanying schedule.

Scheduling Jobs
===============

In the ``config.ts`` file is a section called ``jobs``. There is a property named ``defaultSchedule``. It's value is
used for any subsequent background job which does not have an explicit schedule declared. The value takes a ``cron``
formatted value.

For each background service that is desired to be run with the server, you must specify a new key=>object in the
configuration using the class name. For example, each Composer generated server project comes with a single 
built-in background service called ``MetricsCollector``.

.. code-block:: javascript

    jobs: {
        defaultSchedule: "* * * * * *",
        MetricsCollector: {
            schedule: "*/5 * * * * *",
        },
    },

The above default configuration indicates that the ``MetricsCollector`` background service will be executed once every
5 minutes. The default schedule for all other services is to execute once per minute.

Anatomy of a Background Service
===============================

Each background service class extends the ``BackgroundService`` abstract class and must implement the following three
functions.

* ``run(): void``
* ``start(): Promise<void>``
* ``stop(): Promise<void>``

Note that all functions can optionally return a ``Promise``.

**Example**:

.. code-block:: TypeScript

    /**
     * The `MetricsCollector` provides a background service for collecting Prometheseus metrics for consumption by external
     * clients and compatible servers using the built-in `MetricsRoute` route handler.
     *
     * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
     */
     export default class MetricsCollector extends BackgroundService {
         private registry: prom.Registry;
 
         constructor(config: any, logger: any) {
             super(config, logger);
             this.registry = prom.register;
         }
 
         public run(): void {
             // TODO
         }
 
         public async start(): Promise<void> {}
 
         public async stop(): Promise<void> {}
     }

``run``
~~~~~~~

The ``run`` function is executed on the configured schedule for the background job. It must take no parameters and
must not return any value. It can optionally return a ``Promise`` if the function is asynchronous. All work to be
performed in single pass must be started from this function.

``start``
~~~~~~~~~

At server startup, when all background services are being initialized, the ``start`` function will be executed for
each configured job. This allows for any initialization of defaults such as obtaining database connections, setting
initial values and so on. This function is only ever called once in the server's lifetime.

``stop``
~~~~~~~~

At server shutdown, the ``stop`` function is called for every running background service. This is used to perform any
last minute cleanup that the job may require to exit gracefully.

