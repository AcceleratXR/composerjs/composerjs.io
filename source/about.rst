==============
About Composer
==============

The roots of the Composer.js project date back to 2018 when Jean-Philippe Steinmetz began work on a new MMO gaming
platform called `AcceleratXR <https://goaxr.com>`_. It was decided early on that this project would have to take
a micro-service approach in order to reach levels of scalability and throughput required. Each system or feature
supported by the platform needed to be built quickly, reusing as much code as possible, while providing a high level
of consistency and reliability. It was then that Jean-Philippe decided a code generator tool was the best way to
go.

Upon first discovering OpenAPI Jean-Philippe was very excited, especially at the prospect of pre-made code generation
tools provided by the community. However, these tools quickly proved inadequate to build the complex and mature
platform he was making. So he began to tinker on his own. The first version of Composer (then called ``axr-generator``)
was crude. The tool would copy a complete working service project from a basic template including all service classes
and utilities. This meant that every micro-service project had the same duplicated code. While this was great for
productivity it quickly became an enormous burden for code maintainability. If a bug was found in the core code,
each project had to be re-generated or merged. This was not a sustainable path for development.

Upon making a transition to TypeScript the tools had begun to evolve and take shape. All common functionality were now
placed in libraries that could be added as dependencies and updated with ease. All of the core functionality was
abstracted away from the service itself and leveraged runtime file scanning and decorators to parse the desired
behavior. The code generator tool was modified to only create the minimum files necessary for a given project;
the models and route handlers.

As more and more features were added to the framework it became apparent that the project was becomming unique and
special. So Jean-Philippe and the rest of the AcceleratXR team decided to rename the system to Composer.js and release
it to the community so that everyone can build professional RESTful API services faster, with more consistency and with
enterprise features.